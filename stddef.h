#ifndef _STDDEF_H_
#define _STDDEF_H_

#include "rlibc.h"

#define offsetof(st, m) ((size_t)&(((st *)0)->m))

#endif

